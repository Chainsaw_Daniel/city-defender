/**
 * 
 */
package io.github.brother_daniel.cityDefender.entities;

import io.github.brother_daniel.cityDefender.Game;
import io.github.brother_daniel.cityDefender.Resources;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * @deprecated This is no longer used as it has been replaced by Lasers.
 * 
 */
public class PlayerBeam extends Entity {

	Player player;
	Color beamColour;
	/** The amount to damage enemies while they are getting hurt by this entity */
	float strength;

	public PlayerBeam(Game game) {
		super(game);
	}

	@Override
	public void init() {
		HEIGHT = game.getGameComponent().getHeight() - player.HEIGHT;
		WIDTH = 2;
		beamColour = Color.RED;

		// set strength that will damage enemies
		strength = 1f;

		// Use 0 for y since the y variable is the tip of the Player barrel.
		// Subtract one to center hitbox.
		hitbox = new Rectangle(Math.round(x) - 1, 0, WIDTH, HEIGHT);

		super.init();
	}

	@Override
	public void draw(Graphics2D g) {
		g.setColor(beamColour);
		int xi = Math.round(x);
		int yi = Math.round(y);
		g.drawLine(xi, yi - 1, xi, 0);
		super.draw(g);
	}

	@Override
	public void update() {
		if (player != null) {
			x = player.x + player.WIDTH / 2;
			y = player.y;
			int xi = Math.round(x);
			
			// Use 0 for y since the y variable is the tip of the Player barrel.
			// Subtract one to center hitbox.
			hitbox = new Rectangle(xi - 1, 0, WIDTH, HEIGHT);
			checkCollisions();
		} else {
			System.out.println("Player must be set!");
			if (game.getScene().gameScene) {
				System.exit(Resources.errorCodes.playerBeam_noPlayer);
			} else {
				System.exit(Resources.errorCodes.notGameScene);
			}
		}
		super.update();
	}

	// Check collisions here
	private void checkCollisions() {
		Enemy[] enemies = game.getScene().enemies;
		for (int i = 0; i < enemies.length; i++) {
			if (hitbox.intersects(enemies[i].hitbox)) {
				if (enemies[i].isAlive()) {
					enemies[i].hurt(strength);
					// if the enemy died then...
					if (enemies[i].isAlive() == false) {
						game.getScene().newKill();
					}
				}
			}
		}
	}

	public Player getPlayer() {
		return player;
	}

	/** Used for following the player */
	public void setPlayer(Player player) {
		this.player = player;
	}

}
