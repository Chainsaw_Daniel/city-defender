/**
 * 
 */
package io.github.brother_daniel.cityDefender.entities;

import io.github.brother_daniel.cityDefender.Art;
import io.github.brother_daniel.cityDefender.Game;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Random;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class FastEnemy extends Enemy {

	int hitboxXoff;
	int hitboxYoff;

	public FastEnemy(Game game) {
		super(game);
	}

	@Override
	public void init() {
		WIDTH = 16;
		HEIGHT = 14;
		y = -HEIGHT; // set Position to top
		// Generate random number between GameComponent width - enemy width and 0.
		x = new Random().nextInt(game.getGameComponent().getWidth() - WIDTH * 2);
		dy = 1.5f;
		hitboxXoff = 8;
		hitboxYoff = 17;
		hitbox = new Rectangle(Math.round(x) + hitboxXoff, Math.round(y)
				+ hitboxYoff, WIDTH, HEIGHT);
		life = 40f;
		super.init();
	}

	@Override
	public void draw(Graphics2D g) {
		int xi = Math.round(x);
		int yi = Math.round(y);
		Art.drawImage(g, xi, yi, sprite);
		super.draw(g);
	}

	@Override
	public void update() {
		// Update position of enemy
		y += dy;
		hitbox = new Rectangle(Math.round(x) + hitboxXoff, Math.round(y)
				+ hitboxYoff, WIDTH, HEIGHT);

		super.update();
	}
}
