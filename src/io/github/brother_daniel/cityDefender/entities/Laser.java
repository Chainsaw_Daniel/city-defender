/**
 * 
 */
package io.github.brother_daniel.cityDefender.entities;

import io.github.brother_daniel.cityDefender.Art;
import io.github.brother_daniel.cityDefender.Game;

import java.awt.Graphics2D;
import java.awt.Rectangle;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class Laser extends Entity {

	float speed = 5.0f;
	float strength = 6.0f;

	int hitboxXoff = 2;
	int hitboxYoff = 0;

	boolean markedForDeletion = false;
	boolean visible = true;
	boolean alive = true;
	
	int impactTime;
	int maxImpactTime;
	int curExplosionSprite = 0;
	int[][] explosionSprite;

	public Laser(Game game) {
		super(game);
	}

	@Override
	public void init() {
		super.init();
		WIDTH = sprite.length;
		HEIGHT = sprite[0].length;
		hitbox = new Rectangle(Math.round(x) + hitboxXoff, Math.round(y)
				+ hitboxYoff, WIDTH, HEIGHT);
	}

	@Override
	public void draw(Graphics2D g) {
		super.draw(g);
		Art.drawImage(g, Math.round(x), Math.round(y), sprite);
	}

	@Override
	public void update() {
		super.update();
		// if the laser is off screen
		if (y + HEIGHT < 0){
			visible = false;
			markedForDeletion = true;
		}
		y -= speed;
		// update hitbox.
		hitbox = new Rectangle(Math.round(x) + hitboxXoff, Math.round(y)
				+ hitboxYoff, WIDTH, HEIGHT);
		// hurts intersecting enemies and explodes laser
		checkCollisions();
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	private void checkCollisions() {
		Enemy[] enemies = game.getScene().enemies;
		for (int i = 0; i < enemies.length; i++) {
			if (hitbox.intersects(enemies[i].hitbox)) {
				if (enemies[i].isAlive()) {
					enemies[i].hurt(strength);
					// if the enemy died then...
					if (enemies[i].isAlive() == false) {
						game.getScene().newKill();
					}
					kill();
				}
			}
		}
	}
	
	private void kill() {
		alive = false;
		visible = false;
		markedForDeletion = true;
	}

	public boolean isMarkedForDeletion() {
		return markedForDeletion;
	}

}
