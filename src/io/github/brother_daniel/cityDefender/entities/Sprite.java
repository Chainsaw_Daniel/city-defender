/**
 * 
 */
package io.github.brother_daniel.cityDefender.entities;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 *
 */
public class Sprite {

	int[][] pixels;

	public int[][] getPixels() {
		return pixels;
	}

	public void setPixels(int[][] pixels) {
		this.pixels = pixels;
	}
	
	
	
}
