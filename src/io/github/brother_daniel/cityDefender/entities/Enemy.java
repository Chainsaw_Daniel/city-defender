/**
 * 
 */
package io.github.brother_daniel.cityDefender.entities;

import io.github.brother_daniel.cityDefender.Game;
import io.github.brother_daniel.cityDefender.Resources;
import io.github.brother_daniel.cityDefender.Sound;

import java.io.IOException;

import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class Enemy extends Entity {

	float life = 10;
	boolean alive = true;
	boolean markedForDeletion = false;
	boolean visible = true;
	float hurtTime = 0; // Time left to show hurt tex/snd
	float deathTime = 0;
	float maxDeathTime = 20f;

	// sound variables
	Clip hurtSound;
	long hurtSoundLen;
	Clip deathSound;
	long deathSoundLen;
	long hurtTimeSoundStart;
	Thread sound;
	Sprite[] explosionAnim;

	/**
	 * @param game
	 */
	public Enemy(Game game) {
		super(game);
	}

	@Override
	public void init() {
		// load sounds
		try {
			hurtSound = Sound.loadSound(Resources.resPaths.enemyHurtSound);
			hurtSound.setMicrosecondPosition(hurtSound.getMicrosecondLength());
			hurtSoundLen = hurtSound.getMicrosecondLength();
			deathSound = Sound.loadSound(Resources.resPaths.enemyDeathSound);
			deathSound
					.setMicrosecondPosition(deathSound.getMicrosecondLength());
			// Set it to zero to guarantee sound will play at first
			hurtTimeSoundStart = System.nanoTime() / 1000;
		} catch (LineUnavailableException | UnsupportedAudioFileException
				| IOException e) {
			e.printStackTrace();
		}

		super.init();
	}

	int curExplosionSprite = 0;

	@Override
	public void update() {
		if (deathTime == (int) (maxDeathTime / explosionAnim.length
				* curExplosionSprite + 1)) {
			curExplosionSprite++;
			sprite = explosionAnim[curExplosionSprite - 1].getPixels();
		}
		if (deathTime >= maxDeathTime) {
			visible = false;
			markedForDeletion = true;
		}
		if (alive == false && markedForDeletion == false) {
			deathTime++;
		}
		super.update();
	}

	public float hurt(float damage) {
		life -= damage;
		long curTime = System.nanoTime() / 1000;
		long timePast = curTime - hurtTimeSoundStart;
		// If at beginning of clip or end...
		if (timePast >= hurtSoundLen) {
			// Convert to microseconds
			hurtTimeSoundStart = System.nanoTime() / 1000;
			sound = Sound.playSound(hurtSound);
		}
		// if life is depleted
		if (life <= 0 && alive == true) {
			sprite = explosionAnim[0].getPixels();
			kill();
			// Just in case of errors we will make them stop because we can.
			dy = 0;
		}
		return life;
	}

	public void kill() {
		alive = false;
		life = 0;

		//play sound
		sound = Sound.playSound(deathSound);
	}

	public boolean isMarkedForDeletion() {
		return markedForDeletion;
	}

	public void setMarkedForDeletion(boolean markedForDeletion) {
		this.markedForDeletion = markedForDeletion;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public Sprite[] getExplosionAnim() {
		return explosionAnim;
	}

	public void setExplosionAnim(Sprite[] explosionAnim) {
		this.explosionAnim = explosionAnim;
	}

}
