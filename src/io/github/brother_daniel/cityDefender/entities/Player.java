/**
 * 
 */
package io.github.brother_daniel.cityDefender.entities;

import io.github.brother_daniel.cityDefender.Art;
import io.github.brother_daniel.cityDefender.Game;
import io.github.brother_daniel.cityDefender.Resources;

import java.awt.Graphics2D;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class Player extends Entity {

	// Inputs
	boolean left = false;
	boolean right = false;

	// Move speed whilst shooting.
	float shootingDX = 1.5f;
	
	// Construction site of construct
	public Player(Game game) {
		super(game);
	}

	@Override
	public void init() {
		WIDTH = 32;
		HEIGHT = 32;

		// Set start position
		x = game.getGameComponent().getWidth() / 2 - WIDTH / 2;
		y = game.getGameComponent().getHeight() - HEIGHT;

		// Set speed
		dx = 5.0f;

		sprite = Art.loadImage(Resources.resPaths.player);
		super.init();
	}

	@Override
	public void draw(Graphics2D g) {
		int xi = Math.round(x);
		int yi = Math.round(y);
		Art.drawImage(g, xi, yi, sprite);
		super.draw(g);
	}

	@Override
	public void update() {
		super.update();
	}

	public void processInput(boolean left, boolean right, boolean shooting) {
		
		// check left movement and move
		if (left) {
			if (shooting) {
				x -= shootingDX;
			} else {
				x -= dx;
			}
		}
		
		// Check right movement and move
		if (right) {
			if (shooting) {
				x += shootingDX;
			} else {
				x += dx;
			}
		}
		
		// infinite strafing
		// if touching left side and left key down go to right side.
		if (left && x <= -WIDTH) {
			x = game.getGameComponent().getWidth();
		}
		
		if (right && x >= game.getGameComponent().getWidth()) {
			x = 0;
		}
	}
}
