/**
 * 
 */
package io.github.brother_daniel.cityDefender.entities;

import io.github.brother_daniel.cityDefender.Game;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class Entity {

	Game game;

	int[][] sprite;

	// Position variables
	float x = 0;
	float y = 0;
	/** Speed on the x axis */
	float dx = 1;
	/** Speed on the y axis */
	float dy = 1;

	/** Sprite width. */
	int WIDTH = 32;
	/** Sprite height. */
	int HEIGHT = 32;

	Rectangle2D hitbox;
	int hitboxXOff;
	int hitboxYOff;

	public Entity(Game game) {
		this.game = game;
	}

	public void init() {
	}

	public void draw(Graphics2D g) {
	}

	public void update() {
	}

	public Rectangle2D getHitbox() {
		return hitbox;
	}

	public int[][] getSprite() {
		return sprite;
	}

	public void setSprite(int[][] sprite) {
		this.sprite = sprite;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
}
