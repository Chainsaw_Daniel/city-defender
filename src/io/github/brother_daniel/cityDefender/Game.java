/**
 * 
 */
package io.github.brother_daniel.cityDefender;

import io.github.brother_daniel.cityDefender.scene.MainMenu;
import io.github.brother_daniel.cityDefender.scene.Scene;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import java.io.IOException;

import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class Game {

	GameComponent gc;
	BufferStrategy strategy;
	boolean running = true;
	public boolean isPlaying = true;

	PausePlayInit ppInit;

	InputHandler inputHandler;
	// used to check if pause was pressed down last time.
	private boolean newPause = true;
	public int PAUSE_KEYCODE = Resources.controls.PAUSE_KEYCODE; // P button
	boolean left = false;
	public int LEFT_KEYCODE = Resources.controls.LEFT_KEYCODE; // left arrow
	boolean right = false;
	public int RIGHT_KEYCODE = Resources.controls.RIGHT_KEYCODE; // right arrow
	boolean exit = false;
	public int EXIT_KEYCODE = Resources.controls.EXIT_KEYCODE; // escape key

	Scene scene;

	Clip startSound;

	public Game(GameComponent gc, InputHandler inputHandler) {
		this.gc = gc;
		this.inputHandler = inputHandler;
	}

	/** This is where all game components are initialized. */
	public void init() {
		try {
			startSound = Sound.loadSound(Resources.resPaths.startupSound);
			Sound.playSound(startSound);
		} catch (LineUnavailableException | UnsupportedAudioFileException
				| IOException e) {
			e.printStackTrace();
		}
		setScene(new MainMenu(this));
		scene.init();
	}

	/**
	 * Starts drawing the game <br>
	 * <br>
	 * Warning: The game must be initialized before this is called.
	 */
	public void start() {
		gc.createBufferStrategy(2);
		strategy = gc.getBufferStrategy();

		long lastTime = System.nanoTime();
		final double updatespns = 1000000000.0 / 60.0; // updates per nano
														// second equal
														// to 60 ups
		double delta = 0;

		// TODO FPS limiter
		// game loop
		while (running) {
			long currentTime = System.nanoTime();
			// long elapsedTime = currentTime - lastTime;
			delta += (currentTime - lastTime) / updatespns;
			while (delta >= 1) {
				processInput();
				update();
				delta--;
			}
			draw();

			lastTime = currentTime;
		}

	}

	public void stop() {
		// Prepare for exit
		gc.setVisible(false);
		// TODO Handle errors
		System.exit(0);
	}

	public void processInput() {
		if (inputHandler.keys[EXIT_KEYCODE]) {
			stop();
		}
		//if (isPaused == false) {
			if (inputHandler.keys[LEFT_KEYCODE])
				left = true;
			else
				left = false;

			if (inputHandler.keys[RIGHT_KEYCODE])
				right = true;
			else
				right = false;
			scene.processInput();
		//}
		if (inputHandler.keys[PAUSE_KEYCODE]) {
			if (scene.gameScene && newPause == true) {
				newPause = false;
				togglePause();
			}
		} else if (newPause == false) {
			newPause = true;
		}
	}

	Color backColor = Color.BLACK;

	public void draw() {
		Graphics2D g = (Graphics2D) strategy.getDrawGraphics();
		// Clear Screen
		g.setColor(backColor);
		g.fillRect(0, 0, gc.getWidth(), gc.getHeight());

		scene.draw(g);

		g.dispose();
		strategy.show();
	}

	public void update() {
		scene.update();
	}

	public boolean togglePause() {
		if (isPlaying == false) {
			isPlaying = true;
			ppInit.resume();
		} else {
			isPlaying = false;
			ppInit.pause();
		}
		return isPlaying;
	}

	public Scene getScene() {
		return scene;
	}
	
	public void resetPausePlayListener() {
		ppInit = new PausePlayInit();
		ppInit.addListener(scene);
	}

	public void setScene(Scene scene) {
		this.scene = scene;
		resetPausePlayListener();
	}

	public GameComponent getGameComponent() {
		return gc;
	}

	public InputHandler getInputHandler() {
		return inputHandler;
	}
}
