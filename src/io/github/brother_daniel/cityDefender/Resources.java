/**
 * 
 */
package io.github.brother_daniel.cityDefender;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class Resources {

	public class errorCodes {
		public static final int unknownError = 0;
		public static final int art_textureMissing = 1;
		public static final int playerBeam_noPlayer = 2;
		public static final int notGameScene = 3;
		public static final int isGameScene = 4;
		public static final int userExit = 5;
	}

	public class resPaths {
		// entities
		public static final String player = "/tex/player.png";
		public static final String player2 = "/tex/player2.png";
		public static final String basicEnemySheet = "/tex/enemylvl1.png";
		public static final String fastEnemySheet = "/tex/fastenemy.png";
		public static final String explosion = "/tex/explosion.png";
		public static final String laser = "/tex/laser.png";
		
		// Sounds
		public static final String pauseSound = "/snd/pause.wav";
		public static final String gameOverSound = "/snd/gameover.wav";
		public static final String enemyHurtSound = "/snd/enemyHurt.wav";
		public static final String startupSound = "/snd/startup.wav";
		public static final String enemyDeathSound = "/snd/enemydeath.wav";
		public static final String newWaveSound = "/snd/newwave.wav";
		public static final String menuSelectSound = "/snd/menuselect.wav";
		public static final String menuOpenSound = "/snd/menuopen.wav";
		public static final String laserShoot = "/snd/lasershoot.wav";
	}

	public class fonts {
		public static final String stdFont = "/gui/stdfont.png";
		// @formatter:off
		public static final String stdFontChars = ""
				+ "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
				+ "abcdefghijklmnopqrstuvwxyz"
				+ "0123456789._/\\()<>[]{}+-÷×"
				+ "=:;,!\"'?                  " // =:;,!"'?
				+ "";
		// @formatter:on
		public static final int stdFontlWidth = 6;
		public static final int stdFontlHeight = 10;
		
		public static final String keyFaceFont = "/gui/keyfacefont.png";
		public static final String keyFaceFontChars = "↑→←ÈP";
		public static final int keyFaceFontlWidth = 12;
		public static final int keyFaceFontlHeight = 12;
	}

	public class controls {
		public static final int LEFT_KEYCODE = 37;
		public static final int RIGHT_KEYCODE = 39;
		public static final int SHOOT_KEYCODE = 38;
		public static final int LEFT2P_KEYCODE = 65;
		public static final int RIGHT2P_KEYCODE = 68;
		public static final int SHOOT2P_KEYCODE = 87;
		public static final int RETRY_KEYCODE = 82;
		public static final int EXIT_KEYCODE = 27;
		public static final int PAUSE_KEYCODE = 80; // P
		public static final int MAIN_MENU_KEYCODE = 77;
		public static final int MENU_ENTER_KEYCODE = 32;
		public static final int MENU_DOWN_KEYCODE = 40;
		public static final int MENU_UP_KEYCODE = 38;
	}

}
