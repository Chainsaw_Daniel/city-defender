/**
 * 
 */
package io.github.brother_daniel.cityDefender.wave;

import io.github.brother_daniel.cityDefender.Art;
import io.github.brother_daniel.cityDefender.Game;
import io.github.brother_daniel.cityDefender.Resources;
import io.github.brother_daniel.cityDefender.entities.BasicEnemy;
import io.github.brother_daniel.cityDefender.entities.Enemy;
import io.github.brother_daniel.cityDefender.entities.FastEnemy;
import io.github.brother_daniel.cityDefender.entities.Sprite;

import java.util.Random;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class WaveHandler {
	final double waveMultiplier = 2.25;
	/** Current Wave **/
	int wave;
	/** Time until next enemy will be spawned **/
	int timeNextEnemy;
	/** How many enemies have spawned so far during this wave **/
	int enemiesSpawned;
	/** Enemies left to spawn */
	int enemiesLeft;
	final int gracePeriod = 60 * 15;
	Game game;
	
	// Sprites
	int[][] basicEnemySprite;
	Sprite[] basicEnemyExplosionAnim;
	int[][] fastEnemySprite;
	Sprite[] fastEnemyExplosionAnim;

	public WaveHandler(Game game) {
		this.game = game;
	}

	public void init() {
		wave = 1;
		enemiesSpawned = 0;
		enemiesLeft = wave * (int) waveMultiplier;
		timeNextEnemy = 60 * 5;
		
		// load sprites
		int[][] basicEnemySheet = Art.loadImage(Resources.resPaths.basicEnemySheet);
		basicEnemySprite = Art.extractFromSheet(basicEnemySheet, 32, 32, 0, 0);
		int basicEnemyFrms = 4;
		basicEnemyExplosionAnim = new Sprite[basicEnemyFrms];
		for(int i = 0; i < basicEnemyExplosionAnim.length; i++) {
			basicEnemyExplosionAnim[i] = new Sprite();
			basicEnemyExplosionAnim[i].setPixels(Art.extractFromSheet(basicEnemySheet, 32, 32, i, 0));
		}
		int[][] fastEnemySheet = Art.loadImage(Resources.resPaths.fastEnemySheet);
		fastEnemySprite = Art.extractFromSheet(fastEnemySheet, 32, 32, 0, 0);
		int fastEnemyFrms = 4;
		fastEnemyExplosionAnim = new Sprite[fastEnemyFrms];
		for(int i = 0; i < fastEnemyExplosionAnim.length; i++) {
			fastEnemyExplosionAnim[i] = new Sprite();
			fastEnemyExplosionAnim[i].setPixels(Art.extractFromSheet(fastEnemySheet, 32, 32, i, 0));
		}
	}

	public Enemy[] spawnEnemies(Enemy[] enemies) {
		Enemy[] out = enemies;
		if (enemiesLeft == 0) {
			boolean enemiesDead = true;
			for (int i = 0; i < enemies.length; i++) {
				if (enemies[i].isAlive()) {
					enemiesDead = false;
				}
			}
			if (enemiesDead) {
				incrementWave();
			}
		} else {
			if (timeNextEnemy == 0 && enemiesLeft > 0) {
				int numOfNewEnemies = 1;
				int totalNewEnemies = numOfNewEnemies + enemies.length;
				out = new Enemy[totalNewEnemies];
				for (int i = 0; i < totalNewEnemies; i++) {
					// If we are still setting oldEnemies
					if (i < enemies.length) {
						out[i] = enemies[i];
					} else { // If we are finished with oldEnemies spawn new ones
						Random r = new Random();
						int ri = r.nextInt(4);
						// if the wave is greater than or equal to 2 spawn FastEnemy if random integer (between 0 and 3) equals 0
						// (quarter chance of FastEnemy spawning
						if (ri == 0 && wave >= 2) {
							out[i] = new FastEnemy(game);
							out[i].setSprite(fastEnemySprite);
							out[i].setExplosionAnim(fastEnemyExplosionAnim);

						} else {
							out[i] = new BasicEnemy(game);
							out[i].setSprite(basicEnemySprite);
							out[i].setExplosionAnim(basicEnemyExplosionAnim);
						}
						out[i].init(); // initialize new enemy.
					}
				}
				timeNextEnemy = new Random().nextInt(60 * 30 / wave + 5);
				enemiesSpawned++;
				enemiesLeft--;
			}
			timeNextEnemy--;
		}
		return out;
	}

	public void incrementWave() {
		setWave(getWave() + 1);
		resetEnemiesSpawned();
		// Have a 15 second grace period (if ups = 60).
		timeNextEnemy = gracePeriod;
		enemiesLeft = wave * (int) waveMultiplier;
		game.getScene().newWave();
	}

	public void resetEnemiesSpawned() {
		enemiesSpawned = 0;
	}

	public double getWaveMultiplier() {
		return waveMultiplier;
	}

	public int getWave() {
		return wave;
	}

	public void setWave(int wave) {
		this.wave = wave;
	}

	public int getEnemiesSpawned() {
		return enemiesSpawned;
	}

	public void setEnemiesSpawned(int enemiesSpawned) {
		this.enemiesSpawned = enemiesSpawned;
	}
}
