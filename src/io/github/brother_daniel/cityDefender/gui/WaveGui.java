/**
 * 
 */
package io.github.brother_daniel.cityDefender.gui;

import io.github.brother_daniel.cityDefender.Art;
import io.github.brother_daniel.cityDefender.Game;
import io.github.brother_daniel.cityDefender.Resources;

import java.awt.Graphics2D;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class WaveGui extends Gui {

	int wave;
	int[][] font;
	int fontlWidth;
	int fontlHeight;
	String chars;

	public WaveGui(Game game) {
		super(game);
	}

	@Override
	public void init() {
		x = 0;
		y = 0;
		font = Art.loadImage(Resources.fonts.stdFont);
		chars = Resources.fonts.stdFontChars;
		fontlWidth = Resources.fonts.stdFontlWidth;
		fontlHeight = Resources.fonts.stdFontlHeight;
		wave = 1;
		super.init();
	}

	@Override
	public void draw(Graphics2D g) {
		// Draw wave with font at position.
		String text;
		text = "Wave: " + wave;
		Art.drawString(g, font, fontlWidth, fontlHeight, chars, text, x, y);
		super.draw(g);
	}

	@Override
	public void update() {
		super.update();
	}

	public void newWave() {
		wave++;
	}

	public int getWave() {
		return wave;
	}

	public void setWave(int wave) {
		this.wave = wave;
	}
}
