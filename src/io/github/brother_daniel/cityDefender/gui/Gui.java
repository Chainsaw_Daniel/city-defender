/**
 * 
 */
package io.github.brother_daniel.cityDefender.gui;

import io.github.brother_daniel.cityDefender.Game;

import java.awt.Graphics2D;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class Gui {
	int x = 0;
	int y = 0;

	Game game;

	// @formatter:off
	public Gui(Game game) {
		this.game = game;
	}
	public void init() {}
	public void draw(Graphics2D g) {}
	public void update() {}
	public void processInput(boolean[] keys) {}
	// @formatter:on

	// Coords getters and setters.
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
}
