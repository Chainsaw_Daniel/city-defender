/**
 * 
 */
package io.github.brother_daniel.cityDefender.gui;

import io.github.brother_daniel.cityDefender.Art;
import io.github.brother_daniel.cityDefender.Game;
import io.github.brother_daniel.cityDefender.Resources;

import java.awt.Graphics2D;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class KillCountGui extends Gui {
	int kills = 0;

	String killsVar = "%k";
	String text = "Kills: %k";

	int[][] font;
	int fontlwidth;
	int fontlheight;
	String fontChars;

	int textWidth;
	int textHeight;

	/**
	 * @param game
	 */
	public KillCountGui(Game game) {
		super(game);
	}

	@Override
	public void init() {
		font = Art.loadImage(Resources.fonts.stdFont);
		fontlwidth = Resources.fonts.stdFontlWidth;
		fontlheight = Resources.fonts.stdFontlHeight;
		fontChars = Resources.fonts.stdFontChars;

		textWidth = calcTextWidth();
		textHeight = calcTextHeight();
		// Set position on screen to top right corner.
		x = game.getGameComponent().getWidth() - textWidth;
		y = 0;

		super.init();
	}

	@Override
	public void draw(Graphics2D g) {
		String strDraw = text.replaceAll(killsVar, String.valueOf(kills));
		Art.drawString(g, font, fontlwidth, fontlheight, fontChars, strDraw, x,
				y);
		super.draw(g);
	}

	@Override
	public void update() {
		textWidth = calcTextWidth();
		// Set position on screen to top right corner.
		x = game.getGameComponent().getWidth() - textWidth;
		y = 0;
		super.update();
	}

	public int calcTextWidth() {
		int out = 0;
		String str = text.replaceAll(killsVar, String.valueOf(kills));
		out = fontlwidth * str.length();

		return out;
	}

	public int calcTextHeight() {
		int out = 0;
		// no calculations as currently it is only one line!
		out = fontlheight;

		return out;
	}

	public void incrementKills() {
		kills++;
	}

	public int getKills() {
		return kills;
	}

	public void setKills(int kills) {
		this.kills = kills;
	}

}
