/**
 * 
 */
package io.github.brother_daniel.cityDefender.gui;

import io.github.brother_daniel.cityDefender.Art;
import io.github.brother_daniel.cityDefender.Game;
import io.github.brother_daniel.cityDefender.Resources;
import io.github.brother_daniel.cityDefender.Sound;
import io.github.brother_daniel.cityDefender.gui.MenuList.MenuListener;
import io.github.brother_daniel.cityDefender.scene.MainMenu;
import io.github.brother_daniel.cityDefender.scene.MainScene;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.IOException;

import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 *
 */
public class PauseGui extends Gui implements MenuListener {

	String text = "Paused";

	int enlargeX = 6;
	int enlargeY = 4;

	int[][] stdFont;
	int stdFontlWidth = Resources.fonts.stdFontlWidth;
	int stdFontlHeight = Resources.fonts.stdFontlHeight;

	// Title font
	int[][] font;
	int fontlwidth = stdFontlWidth * enlargeX;
	int fontlheight = stdFontlHeight * enlargeY;
	String fontChars = Resources.fonts.stdFontChars;

	MenuList menuList;
	String[] menuItems = { "Return to Game", "Retry", "Return to Main Menu" };
	int menuItemsX;
	int menuItemsY;
	Clip menuSelectSound;
	Clip menuOpenSound;

	public int numOfPlayers = 1;

	public PauseGui(Game game) {
		super(game);

	}

	@Override
	public void init() {
		stdFont = Art.loadImage(Resources.fonts.stdFont);
		font = Art.enlargeImage(stdFont, enlargeX, enlargeY);

		//menuList init
		menuList = new MenuList(game);
		menuList.setKEYCODE_DOWN(Resources.controls.MENU_DOWN_KEYCODE);
		menuList.setKEYCODE_UP(Resources.controls.MENU_UP_KEYCODE);
		menuList.setKEYCODE_ENTER(Resources.controls.MENU_ENTER_KEYCODE);
		menuList.setMenuFont(stdFont);
		menuList.setMenuFontCharacters(fontChars);
		menuList.setMenuFontlheight(stdFontlHeight);
		menuList.setMenuFontlwidth(stdFontlWidth);
		menuList.setMenuFontSelected(Art.changeColor(stdFont, Color.WHITE,
				Color.ORANGE));
		menuList.setMenuItems(menuItems);
		// calc menuItems x & y
		menuItemsX = game.getGameComponent().getWidth() / 2;
		int i = fontlheight * (menuItems.length - 1) - 2;
		menuItemsY = ((game.getGameComponent().getHeight() - i) / 4) * 3;
		menuList.setX(menuItemsX);
		menuList.setY(menuItemsY);
		menuList.init();
		menuList.menuThrower.addThrowListener(this);

		try {
			menuSelectSound = Sound
					.loadSound(Resources.resPaths.menuSelectSound);
			menuOpenSound = Sound.loadSound(Resources.resPaths.menuOpenSound);
		} catch (LineUnavailableException | UnsupportedAudioFileException
				| IOException e) {
			e.printStackTrace();
		}

		super.init();
	}

	@Override
	public void draw(Graphics2D g) {
		Art.drawCenterString(g, font, fontlwidth, fontlheight, fontChars, text,
				x, y);
		menuList.draw(g);
		super.draw(g);
	}

	@Override
	public void update() {
		menuList.update();
		super.update();
	}

	@Override
	public void processInput(boolean[] keys) {
		menuList.processInput(keys);
		super.processInput(keys);
	}

	@Override
	public void menuEnter() {
		Sound.playSound(menuOpenSound);
		int item = menuList.getMenuItemSelect();

		switch (item) {
		case 0:
			game.isPlaying = true;
			break;
		case 1:
			MainScene mainScene = new MainScene(game);
			game.setScene(mainScene);
			mainScene.setNumOfPlayers(numOfPlayers);
			game.getScene().init();
			game.isPlaying = true;
			break;
		case 2:
			game.setScene(new MainMenu(game));
			game.getScene().init();
			game.isPlaying = true;
			break;
		}
	}

	@Override
	public void menuSelect() {
		Sound.playSound(menuSelectSound);
	}

}
