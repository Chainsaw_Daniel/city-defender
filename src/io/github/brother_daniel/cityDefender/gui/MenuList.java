/**
 * 
 */
package io.github.brother_daniel.cityDefender.gui;

import io.github.brother_daniel.cityDefender.Art;
import io.github.brother_daniel.cityDefender.Game;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class MenuList extends Gui {
	String[] menuItems;
	int menuItemSelect;
	// font
	int[][] menuFont;
	int[][] menuFontSelected;
	int menuFontlwidth;
	int menuFontlheight;
	String menuFontCharacters;
	int menuFontLineSpace = 10;

	// inputs
	int KEYCODE_UP;
	int KEYCODE_DOWN;
	int KEYCODE_ENTER;
	boolean keyUpDown = false;
	boolean keyDownDown = false;
	boolean keyEnterDown = false;
	boolean ignoreEnterDown = true;
	
	public MenuThrower menuThrower;

	public MenuList(Game game) {
		super(game);
	}

	@Override
	public void init() {
		super.init();
		menuThrower = new MenuThrower();
		
	}

	@Override
	public void draw(Graphics2D g) {
		super.draw(g);
		// draw menu items
		for (int i = 0; i < menuItems.length; i++) {
			if (i == menuItemSelect) {
				Art.drawCenterString(g, menuFontSelected, menuFontlwidth,
						menuFontlheight, menuFontCharacters, menuItems[i],
						x, y + i * (menuFontlheight + menuFontLineSpace));
			} else {
				Art.drawCenterString(g, menuFont, menuFontlwidth,
						menuFontlheight, menuFontCharacters, menuItems[i],
						x, y + i * (menuFontlheight + menuFontLineSpace));
			}
		}
	}

	@Override
	public void update() {
		super.update();
	}

	// The best speed at which it doesn't screw with
	// the audio.
	int cycleSpeed = 22;
	int inputProcess = cycleSpeed;
	
	@Override
	public void processInput(boolean[] keys) {
		super.processInput(keys);
		// get game InputHandler
		keyDownDown = keys[KEYCODE_DOWN];
		keyUpDown = keys[KEYCODE_UP];
		keyEnterDown = keys[KEYCODE_ENTER];

		if (keyDownDown) {
			if (inputProcess >= cycleSpeed) {
				inputProcess = 0;
				// cycle down through menus
				// if menu is at the bottom
				if (menuItemSelect == menuItems.length - 1) {
					// select the top item.
					menuItemSelect = 0;
				} else { // every other time
					menuItemSelect++;
				}

				menuThrower.throwSelect();
			}
		}
		if (keyUpDown) {
			if (inputProcess >= cycleSpeed) {
				inputProcess = 0;
				// cycle up through menus
				// if menu is at the bottom
				if (menuItemSelect == 0) {
					// select the top item.
					menuItemSelect = menuItems.length - 1;
				} else { // every other time
					menuItemSelect--;
				}

				menuThrower.throwSelect();
			}
		}
		if (ignoreEnterDown && keyEnterDown == false) {
			ignoreEnterDown = false;
		}
		if (keyEnterDown && ignoreEnterDown == false) {
			menuThrower.throwEnter();
		}
		inputProcess++;
	}

	// getters and setters

	public String[] getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(String[] menuItems) {
		this.menuItems = menuItems;
	}

	public int[][] getMenuFont() {
		return menuFont;
	}

	public void setMenuFont(int[][] menuFont) {
		this.menuFont = menuFont;
	}

	public int[][] getMenuFontSelected() {
		return menuFontSelected;
	}

	public void setMenuFontSelected(int[][] menuFontSelected) {
		this.menuFontSelected = menuFontSelected;
	}

	public String getMenuFontCharacters() {
		return menuFontCharacters;
	}

	public void setMenuFontCharacters(String menuFontCharacters) {
		this.menuFontCharacters = menuFontCharacters;
	}

	public int getMenuItemSelect() {
		return menuItemSelect;
	}

	public void setMenuItemSelect(int menuItemSelect) {
		this.menuItemSelect = menuItemSelect;
	}

	public int getMenuFontlwidth() {
		return menuFontlwidth;
	}

	public void setMenuFontlwidth(int menuFontlwidth) {
		this.menuFontlwidth = menuFontlwidth;
	}

	public int getMenuFontlheight() {
		return menuFontlheight;
	}

	public void setMenuFontlheight(int menuFontlheight) {
		this.menuFontlheight = menuFontlheight;
	}

	public int getKEYCODE_UP() {
		return KEYCODE_UP;
	}

	public void setKEYCODE_UP(int kEYCODE_UP) {
		KEYCODE_UP = kEYCODE_UP;
	}

	public int getKEYCODE_DOWN() {
		return KEYCODE_DOWN;
	}

	public void setKEYCODE_DOWN(int kEYCODE_DOWN) {
		KEYCODE_DOWN = kEYCODE_DOWN;
	}

	public int getKEYCODE_ENTER() {
		return KEYCODE_ENTER;
	}

	public void setKEYCODE_ENTER(int kEYCODE_ENTER) {
		KEYCODE_ENTER = kEYCODE_ENTER;
	}

	public int getMenuFontLineSpace() {
		return menuFontLineSpace;
	}

	public void setMenuFontLineSpace(int menuFontLineSpace) {
		this.menuFontLineSpace = menuFontLineSpace;
	}

	public interface MenuListener {
		public void menuEnter();
		public void menuSelect();
	}

	public class MenuThrower {
		List<MenuListener> listeners = new ArrayList<MenuListener>();

		public void addThrowListener(MenuListener toAdd) {
			listeners.add(toAdd);
		}

		// event throwers
		public void throwEnter() {
			for (MenuListener menuListener : listeners)
				menuListener.menuEnter();
		}
		public void throwSelect() {
			for (MenuListener menuListener : listeners)
				menuListener.menuSelect();
		}
	}

}
