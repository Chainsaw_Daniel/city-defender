/**
 * 
 */
package io.github.brother_daniel.cityDefender.scene;

import io.github.brother_daniel.cityDefender.Art;
import io.github.brother_daniel.cityDefender.Game;
import io.github.brother_daniel.cityDefender.GameComponent;
import io.github.brother_daniel.cityDefender.InputHandler;
import io.github.brother_daniel.cityDefender.Resources;
import io.github.brother_daniel.cityDefender.Sound;
import io.github.brother_daniel.cityDefender.gui.MenuList;
import io.github.brother_daniel.cityDefender.gui.MenuList.MenuListener;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.IOException;

import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class MainMenu extends Scene implements MenuListener {

	boolean keyDownDown = false;
	int keyDown = Resources.controls.MENU_DOWN_KEYCODE;
	boolean keyUpDown = false;
	int keyUp = Resources.controls.MENU_UP_KEYCODE;
	boolean keyExitDown = false;
	int keyExit = Resources.controls.EXIT_KEYCODE;
	boolean keyEnterDown = false;
	int keyEnter = Resources.controls.MENU_ENTER_KEYCODE;

	String[] menuItems = { "1 Player Game Start", "2 Player Game Start",
			"How to Play", "Exit Game" };
	int menuItemSelect = 0;

	String[] controlsText = { "Select menus: Arrow keys", "Enter menu: Space" };
	int controlsTextX = 0;
	int controlsTextY = 0;

	int[][] font;
	int fontlwidth;
	int fontlheight;
	String fontCharacters;
	int textX;
	int textY;

	int[][] menuFont;
	int[][] menuFontSelected;
	int menuFontlwidth;
	int menuFontlheight;
	int menuFontRatio = 2;

	MenuList menuList;
	MenuListener menuListener;

	int[][] titleFont;
	int titleFontRatio = 6;
	int titleFontlwidth;
	int titleFontlheight;
	String title;
	int titleX;
	int titleY;

	// Sounds
	Clip menuSelect;
	Clip menuOpen;

	public MainMenu(Game game) {
		super(game);
	}

	@Override
	public void init() {
		// Let objects in game know that this is not a GameScence.
		gameScene = false;

		// load textures.
		// generic font
		font = Art.loadImage(Resources.fonts.stdFont);
		fontlwidth = Resources.fonts.stdFontlWidth;
		fontlheight = Resources.fonts.stdFontlHeight;
		fontCharacters = Resources.fonts.stdFontChars;
		// menu item font
		menuFontlwidth = fontlwidth * menuFontRatio;
		menuFontlheight = fontlheight * menuFontRatio;
		menuFont = Art.enlargeImage(font, menuFontRatio);
		menuFontSelected = Art.changeColor(menuFont, Color.WHITE, Color.ORANGE);
		textX = game.getGameComponent().getWidth() / 2;
		int i = fontlheight * (menuItems.length - 1) - 2;
		textY = (game.getGameComponent().getHeight() - i) / 2;
		menuItemSelect = 0;
		// title
		title = GameComponent.getGameTitle();
		titleFont = Art.enlargeImage(font, titleFontRatio);
		titleFontlwidth = fontlwidth * titleFontRatio;
		titleFontlheight = fontlheight * titleFontRatio;
		titleX = GameComponent.WIDTH / 2;
		titleY = GameComponent.HEIGHT / 4;
		// load sounds
		try {
			menuSelect = Sound.loadSound(Resources.resPaths.menuSelectSound);
			menuOpen = Sound.loadSound(Resources.resPaths.menuOpenSound);
		} catch (LineUnavailableException | UnsupportedAudioFileException
				| IOException e) {
			e.printStackTrace();
		}

		// FOR TESTING ONLY
		menuList = new MenuList(game);
		menuList.init();

		menuList.menuThrower.addThrowListener(this);

		menuList.setKEYCODE_DOWN(keyDown);
		menuList.setKEYCODE_ENTER(keyEnter);
		menuList.setKEYCODE_UP(keyUp);

		menuList.setMenuFont(menuFont);
		menuList.setMenuFontlheight(menuFontlheight);
		menuList.setMenuFontlwidth(menuFontlwidth);
		menuList.setMenuFontSelected(menuFontSelected);
		menuList.setMenuItems(menuItems);
		menuList.setMenuFontCharacters(fontCharacters);

		menuList.setX(textX);
		menuList.setY(textY);

		// TODO fix hard code here!!!
		controlsTextY = GameComponent.HEIGHT - fontlheight * 5;

		super.init();
	}

	@Override
	public void draw(Graphics2D g) {
		menuList.draw(g);

		// For debug purposes when finding position of center of screen
		//g.drawRect(game.getGameComponent().getWidth() / 2, game.getGameComponent().getHeight() / 2, 2000, 2000);

		// draw title
		Art.drawCenterString(g, titleFont, titleFontlwidth, titleFontlheight,
				fontCharacters, title, titleX, titleY);
		for (int i = 0; i < controlsText.length; i++) {
			Art.drawString(g, font, fontlwidth, fontlheight, fontCharacters,
					controlsText[i], controlsTextX, controlsTextY + fontlheight * i);
		}
		//g.drawRect(0, controlsTextY, 100, 100);
		super.draw(g);
	}

	@Override
	public void update() {
		super.update();
	}

	int inputProcess = 15;

	@Override
	public void processInput() {
		// get game InputHandler
		InputHandler inputHandler = game.getInputHandler();
		keyDownDown = inputHandler.keys[keyDown];
		keyUpDown = inputHandler.keys[keyUp];
		keyEnterDown = inputHandler.keys[keyEnter];
		keyExitDown = inputHandler.keys[keyExit];

		if (keyExitDown) {
			System.exit(Resources.errorCodes.userExit);
		}
		menuList.processInput(inputHandler.keys);

		inputProcess++;
		super.processInput();
	}

	private void openMenuItem(int item) {
		if (item == 0) {
			Scene scene = new MainScene(game);
			scene.init();
			game.setScene(scene);
		}
		if (item == 1) { // 2nd item                        
			// play 2 players                                         
			MainScene mainScene = new MainScene(game);
			// set to a 2 player game                                 
			mainScene.setNumOfPlayers(2);
			mainScene.init();
			game.setScene(mainScene);
		}
		if (item == 2) { // 3rd item
			// Show instructions
			InstructionsScene instructScene = new InstructionsScene(game);
			instructScene.init();
			game.setScene(instructScene);
		}
		if (item == menuItems.length - 1) { // the last item
			System.exit(Resources.errorCodes.userExit);
		}
	}

	@Override
	public void menuEnter() {
		Sound.playSound(menuOpen);
		int menuItemSelected = menuList.getMenuItemSelect();
		openMenuItem(menuItemSelected);
	}

	@Override
	public void menuSelect() {
		// play menu select sound.
		Sound.playSound(menuSelect);
	}

}
