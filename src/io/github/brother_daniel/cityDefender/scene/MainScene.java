/**
 * 
 */
package io.github.brother_daniel.cityDefender.scene;

import io.github.brother_daniel.cityDefender.Art;
import io.github.brother_daniel.cityDefender.Game;
import io.github.brother_daniel.cityDefender.Resources;
import io.github.brother_daniel.cityDefender.Sound;
import io.github.brother_daniel.cityDefender.entities.Enemy;
import io.github.brother_daniel.cityDefender.entities.Laser;
import io.github.brother_daniel.cityDefender.entities.Player;
import io.github.brother_daniel.cityDefender.gui.Gui;
import io.github.brother_daniel.cityDefender.gui.KillCountGui;
import io.github.brother_daniel.cityDefender.gui.PauseGui;
import io.github.brother_daniel.cityDefender.gui.WaveGui;
import io.github.brother_daniel.cityDefender.wave.WaveHandler;

import java.awt.Graphics2D;
import java.io.IOException;
import java.util.ArrayList;

import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class MainScene extends Scene {

	boolean twoPlayer = false;
	Player[] player;

	ArrayList<Laser> lasers = new ArrayList<Laser>();
	int[][] laserSprite;
	boolean p1ShootLaser = true;
	boolean p2ShootLaser = true;

	int laserShotDelay = 15;
	int p1LastLaserShot = laserShotDelay;
	int p2LastLaserShot = laserShotDelay;

	WaveHandler waveHandler;
	WaveGui waveGui;
	KillCountGui killGui;
	PauseGui pauseGui;

	Clip newWaveSound;
	Clip laserShootSound;
	Clip pauseSound;

	public MainScene(Game game) {
		super(game);
	}

	@Override
	public void init() {
		// Let entities and other objects know that this is not a menu.
		gameScene = true;

		// init gui
		gui = new Gui[0];
		waveGui = new WaveGui(game);
		waveGui.init();
		killGui = new KillCountGui(game);
		killGui.init();
		pauseGui = new PauseGui(game);
		pauseGui.init();
		pauseGui.setX(game.getGameComponent().getWidth() / 2);
		pauseGui.setY(game.getGameComponent().getHeight() / 2);
		if (twoPlayer) {
			pauseGui.numOfPlayers = 2;
		} else {
			pauseGui.numOfPlayers = 1;
		}

		// init entities/players
		if (twoPlayer) {
			// set number of players
			player = new Player[2];
		} else {
			// set number of players
			player = new Player[1];
		}
		// player 0 is player 1
		player[0] = new Player(game);
		player[0].init();
		laserSprite = Art.loadImage(Resources.resPaths.laser);

		// if two player mode is on then
		if (twoPlayer) {
			// player 1 is player 2
			player[1] = new Player(game);
			player[1].init();
			player[1].setSprite(Art.loadImage(Resources.resPaths.player2));
		}

		enemies = new Enemy[0];

		waveHandler = new WaveHandler(game);
		waveHandler.init();
		waveGui.setWave(waveHandler.getWave());

		// load sounds
		try {
			newWaveSound = Sound.loadSound(Resources.resPaths.newWaveSound);
			laserShootSound = Sound.loadSound(Resources.resPaths.laserShoot);
			pauseSound = Sound.loadSound(Resources.resPaths.pauseSound);
		} catch (LineUnavailableException | UnsupportedAudioFileException
				| IOException e) {
			e.printStackTrace();
		}

		super.init();
	}

	@Override
	public void draw(Graphics2D g) {
		if (game.isPlaying) {
			// draw enemies
			for (int i = 0; i < enemies.length; i++) {
				if (enemies[i].isVisible()) {
					enemies[i].draw(g);
				}
			}

			// draw lasers
			for (int i = 0; i < lasers.size(); i++) {
				if (lasers.get(i).isMarkedForDeletion() == false
						&& lasers.get(i).isVisible()) {
					lasers.get(i).draw(g);
				}
			}
		} else {
			pauseGui.draw(g);
		}

		// draw players
		player[0].draw(g);
		if (twoPlayer) {
			player[1].draw(g);
		}

		// draw gui
		for (int i = 0; i < gui.length; i++) {
			gui[i].draw(g);
		}
		waveGui.draw(g);
		killGui.draw(g);

		super.draw(g);
	}

	// clean every 30 seconds if game is at 60 ups
	int maxCleanTick = 60 * 30;
	int cleanTickCounter = 0;

	@Override
	public void update() {
		super.update();

		if (game.isPlaying) {
			// update players
			player[0].update();
			if (twoPlayer) {
				player[1].update();
			}

			// update lasers
			for (int i = 0; i < lasers.size(); i++) {
				if (lasers.get(i).isMarkedForDeletion() == false) {
					lasers.get(i).update();
				}
			}

			// shoot lasers
			if (p1ShootLaser && p1LastLaserShot > laserShotDelay) {
				shootLaser(1);
				p1LastLaserShot = 0;
			}
			if (twoPlayer) {
				if (p2ShootLaser && p2LastLaserShot > laserShotDelay) {
					shootLaser(2);
					p2LastLaserShot = 0;
				}
				p2LastLaserShot++;
			}
			p1LastLaserShot++;

			// update enemies
			for (int i = 0; i < enemies.length; i++) {
				if (enemies[i].isMarkedForDeletion() == false) {
					enemies[i].update();
				}
			}

			// update gui
			for (int i = 0; i < gui.length; i++) {
				gui[i].update();
			}
			waveGui.update();
			killGui.update();

			// check for Game Over
			if (checkGameOver()) {
				gameOver();
			}

			enemies = waveHandler.spawnEnemies(enemies);
			cleanTickCounter++;
			// If its time to clean the dead entities
			// Increase tick by 1
			if (cleanTickCounter == maxCleanTick) {
				cleanTickCounter = 0;
				disposeOldEnemies(enemies);
				disposeOldLasers();
				System.out.println("Cleaning up");
			}
		} else {
			pauseGui.update();
		}
	}

	@Override
	public void onPause() {
		Sound.playSound(pauseSound);
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void processInput() {
		if (game.isPlaying) {
			boolean left = game.getInputHandler().keyBits
					.get(game.LEFT_KEYCODE);
			boolean right = game.getInputHandler().keyBits
					.get(game.RIGHT_KEYCODE);
			boolean left2P = game.getInputHandler().keyBits
					.get(Resources.controls.LEFT2P_KEYCODE);
			boolean right2P = game.getInputHandler().keyBits
					.get(Resources.controls.RIGHT2P_KEYCODE);
			player[0].processInput(left, right, game.getInputHandler().keyBits
					.get(Resources.controls.SHOOT_KEYCODE));
			if (twoPlayer) {
				player[1].processInput(left2P, right2P,
						game.getInputHandler().keyBits
								.get(Resources.controls.SHOOT2P_KEYCODE));
			}
			if (game.getInputHandler().keyBits
					.get(Resources.controls.SHOOT_KEYCODE)) {
				p1ShootLaser = true;
			} else
				p1ShootLaser = false;

			if (game.getInputHandler().keyBits
					.get(Resources.controls.SHOOT2P_KEYCODE)) {
				p2ShootLaser = true;
			} else {
				p2ShootLaser = false;
			}
		} else {
			pauseGui.processInput(game.getInputHandler().keys);
		}
		super.processInput();
	}

	public Enemy[] disposeOldEnemies(Enemy[] in) {
		Enemy[] out = new Enemy[1];
		// TODO fix possible performance issue here using Iterators
		for (int i = 0; i < in.length; i++) {
			if (in[i].isMarkedForDeletion()) {
				Enemy[] old = out; // Backup old out.
				out = new Enemy[i + 1]; // Create new out.
				for (int i2 = 0; i2 < old.length; i2++) {
					out[i2] = old[i2]; // Re-add old items
				}
			}
		}
		return out;
	}

	public void disposeOldLasers() {
		for (int i = 0; i < lasers.size(); i++) {
			if (lasers.get(i).isMarkedForDeletion()) {
				lasers.remove(i);
			}
		}
	}

	public void shootLaser(int player) {
		Laser newLaser = new Laser(game);
		newLaser.setSprite(laserSprite);
		newLaser.init();
		if (player == 1) {
			newLaser.setX(this.player[0].getX()
					+ this.player[0].getSprite().length / 2 - 4);
			newLaser.setY(game.getGameComponent().getHeight()
					- this.player[0].getSprite()[0].length);
		} else if (player == 2) {
			newLaser.setX(this.player[1].getX()
					+ this.player[1].getSprite().length / 2 - 4);
			newLaser.setY(game.getGameComponent().getHeight()
					- this.player[1].getSprite()[0].length);
		}
		lasers.add(newLaser);
		Sound.playSound(laserShootSound);
	}

	public boolean checkGameOver() {
		for (int i = 0; i < enemies.length; i++) {
			if (enemies[i].isAlive()) {
				int enemyHeight = (int) enemies[i].getHitbox().getHeight();
				int enemyY = (int) enemies[i].getHitbox().getY();
				int gameHeight = game.getGameComponent().getHeight();
				if (enemyY >= gameHeight - enemyHeight) {
					return true;
				}
			}
		}
		return false;
	}

	public void gameOver() {
		GameOver gameOver = new GameOver(game);
		gameOver.setWave(waveHandler.getWave());
		gameOver.setKills(killGui.getKills());
		// Check if two players for retry
		if (twoPlayer) {
			gameOver.numOfPlayers = 2;
		}
		gameOver.init();
		game.setScene(gameOver);
	}

	@Override
	public void newWave() {
		waveGui.newWave();
		Sound.playSound(newWaveSound);
		super.newWave();
	}

	@Override
	public void newKill() {
		// increment kill in the killCountGui
		killGui.incrementKills();
		super.newKill();
	}

	public void setNumOfPlayers(int i) {
		if (i == 1)
			twoPlayer = false;
		else if (i == 2)
			twoPlayer = true;
		else
			System.exit(0);
	}
}
