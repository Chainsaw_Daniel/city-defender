/**
 * 
 */
package io.github.brother_daniel.cityDefender.scene;

import io.github.brother_daniel.cityDefender.Game;
import io.github.brother_daniel.cityDefender.PausePlayListener;
import io.github.brother_daniel.cityDefender.entities.Enemy;
import io.github.brother_daniel.cityDefender.gui.Gui;

import java.awt.Graphics2D;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class Scene implements PausePlayListener {

	Game game;
	public boolean gameScene;

	public Enemy[] enemies;
	public Gui[] gui;

	public Scene(Game game) {
		this.game = game;
	}

	public void init() {
	}

	public void draw(Graphics2D g) {
	}

	public void update() {
	}

	public void processInput() {
	}

	public void newWave() {
	}

	public void newKill() {
	}

	@Override
	public void onResume() {
	}

	@Override
	public void onPause() {
	}

}
