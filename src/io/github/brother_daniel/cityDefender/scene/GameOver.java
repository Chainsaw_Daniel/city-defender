package io.github.brother_daniel.cityDefender.scene;

import io.github.brother_daniel.cityDefender.Art;
import io.github.brother_daniel.cityDefender.Game;
import io.github.brother_daniel.cityDefender.Resources;
import io.github.brother_daniel.cityDefender.Sound;
import io.github.brother_daniel.cityDefender.gui.MenuList;
import io.github.brother_daniel.cityDefender.gui.MenuList.MenuListener;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.IOException;

import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class GameOver extends Scene implements MenuListener {

//	boolean keyRetryDown = false;
//	int keyRetry = Resources.controls.RETRY_KEYCODE;
//	boolean keyExitDown = false;
//	int keyMenu = Resources.controls.MAIN_MENU_KEYCODE;

	int[][] font;
	int fontlHeight;
	int fontlWidth;
	String fontChars;
	// text font
	int[][] textFont;
	int textFontlHeight;
	int textFontlWidth;
	String waveVar = "%w";
	String killsVar = "%k";
	String[] text = { "You lasted %w waves!", "You killed %k enemies!",
			"Retry", "Return to main menu" };
	int textPosX;
	int textPosY;
	int textFontRatio = 2;
	// title font
	String title = "Game Over! :(";
	int titleX = 0;
	int titleY = 0;
	int[][] titleFont;
	int titleFontlwidth;
	int titleFontlheight;
	int titleRatio = 6;

	int wave = 1;
	int kills = 0;

	MenuList menuList;
	
	// sounds
	Clip menuSelectSound;
	Clip menuEnterSound;

	public boolean retryMultiplayer = false;
	public int numOfPlayers = 1;
	
	public GameOver(Game game) {
		super(game);
	}

	@Override
	public void init() {
		// Initialize scene/load files.
		gameScene = false;
		font = Art.loadImage(Resources.fonts.stdFont);
		fontlWidth = Resources.fonts.stdFontlWidth;
		fontlHeight = Resources.fonts.stdFontlHeight;
		fontChars = Resources.fonts.stdFontChars;
		// text stuff
		textFont = Art.enlargeImage(font, textFontRatio);
		textFontlWidth = fontlWidth * textFontRatio;
		textFontlHeight = fontlHeight * textFontRatio;
		// set position
		textPosX = game.getGameComponent().getWidth() / 2;
		int i = textFontlHeight * (text.length - 1) - 2;//- 1) - 2;
		textPosY = (game.getGameComponent().getHeight() - i) / 2;

		menuList = new MenuList(game);

		for (int i1 = 0; i1 < text.length; i1++) {
			String str = text[i1].replaceAll(waveVar, String.valueOf(wave));
			str = str.replaceAll(killsVar, String.valueOf(kills));
			text[i1] = str;
		}

		menuList.setKEYCODE_DOWN(Resources.controls.MENU_DOWN_KEYCODE);
		menuList.setKEYCODE_UP(Resources.controls.MENU_UP_KEYCODE);
		menuList.setKEYCODE_ENTER(Resources.controls.MENU_ENTER_KEYCODE);
		menuList.setMenuFont(textFont);
		menuList.setMenuFontCharacters(fontChars);
		menuList.setMenuFontlheight(textFontlHeight);
		menuList.setMenuFontlwidth(textFontlWidth);
		menuList.setMenuFontSelected(Art.changeColor(textFont, Color.WHITE,
				Color.ORANGE));
		menuList.setMenuItems(text);
		menuList.setX(textPosX);
		menuList.setY(textPosY);

		menuList.init();
		menuList.menuThrower.addThrowListener(this);

		// title stuff
		// position x at center
		titleX = game.getGameComponent().getWidth() / 2;
		// Quarter way down
		titleY = game.getGameComponent().getHeight() / 4;
		titleFont = Art.enlargeImage(font, titleRatio);
		titleFont = Art.changeColor(titleFont, Color.WHITE, Color.RED);
		titleFontlwidth = fontlWidth * titleRatio;
		titleFontlheight = fontlHeight * titleRatio;

		String gameOverSoundPath = Resources.resPaths.gameOverSound;
		try {
			Clip gameOverSound = Sound.loadSound(gameOverSoundPath);
			Sound.playSound(gameOverSound);
			menuSelectSound = Sound.loadSound(Resources.resPaths.menuSelectSound);
			menuEnterSound = Sound.loadSound(Resources.resPaths.menuOpenSound);
		} catch (LineUnavailableException | UnsupportedAudioFileException
				| IOException e) {
			System.out.println("unable to load sound");
			e.printStackTrace();
		}

		super.init();
	}

	@Override
	public void draw(Graphics2D g) {
		//		for (int i = 0; i < text.length; i++) {
		//			String str = text[i].replaceAll(waveVar, String.valueOf(wave));
		//			str = str.replaceAll(killsVar, String.valueOf(kills));
		//			//			Art.drawString(g, font, fontlWidth, fontlHeight, fontChars, str,
		//			//					textPosX, textPosY + fontlHeight * i);
		//			Art.drawCenterString(g, textFont, textFontlWidth, textFontlHeight, fontChars,
		//					str, textPosX, textPosY + i * textFontlHeight);
		//		}
		menuList.draw(g);

		// Draw Game Over.
		Art.drawCenterString(g, titleFont, titleFontlwidth, titleFontlheight,
				fontChars, title, titleX, titleY);

		super.draw(g);
	}

	@Override
	public void update() {
		menuList.update();
		super.update();
	}

	@Override
	public void processInput() {
		// Process user inputs.
		//		InputHandler inputHandler = game.getInputHandler();
		//		if (inputHandler.keys[keyMenu]) {
		//			MainMenu mainMenu = new MainMenu(game);
		//			mainMenu.init();
		//			game.setScene(mainMenu);
		//		}
		//		if (inputHandler.keys[keyRetry]) {
		//			// Create new MainScene scene.
		//			MainScene mainScene = new MainScene(game);
		//			mainScene.init();
		//			game.setScene(mainScene);
		//		}

		menuList.processInput(game.getInputHandler().keys);

		super.processInput();
	}

	public int getWave() {
		return wave;
	}

	public void setWave(int wave) {
		this.wave = wave;
	}

	public int getKills() {
		return kills;
	}

	public void setKills(int kills) {
		this.kills = kills;
	}
	
	private void openMenuItem(int item) {
		if (item == 2) {
			Sound.playSound(menuEnterSound);
			MainScene mainScene = new MainScene(game);
			game.setScene(mainScene);
			mainScene.setNumOfPlayers(numOfPlayers);
			game.getScene().init();
		}
		if (item == 3) {
			Sound.playSound(menuEnterSound);
			game.setScene(new MainMenu(game));
			game.getScene().init();
		}
	}

	@Override
	public void menuEnter() {
		openMenuItem(menuList.getMenuItemSelect());
	}

	@Override
	public void menuSelect() {
		Sound.playSound(menuSelectSound);
	}

}
