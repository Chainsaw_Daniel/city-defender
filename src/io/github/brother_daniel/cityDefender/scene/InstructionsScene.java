/**
 * 
 */
package io.github.brother_daniel.cityDefender.scene;

import java.awt.Graphics2D;

import io.github.brother_daniel.cityDefender.Art;
import io.github.brother_daniel.cityDefender.Game;
import io.github.brother_daniel.cityDefender.GameComponent;
import io.github.brother_daniel.cityDefender.Resources;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 *
 */
public class InstructionsScene extends Scene {
	
	// FONT VARIABLES
	// STD FONT
	int[][] font;
	int fontlwidth;
	int fontlheight;
	String fontCharacters;
	int textX;
	int textY;
	// TITLE FONT
	int[][] titleFont;
	int titleFontRatio = 6;
	int titleFontlwidth;
	int titleFontlheight;
	String title = "How To Play";
	int titleX;
	int titleY;

	public InstructionsScene(Game game) {
		super(game);
	}
	
	@Override
	public void init() {
		// So the game engine knows this is not a game scene
		gameScene = false;
		
		// Load textures
		// Load stdFont
		font = Art.loadImage(Resources.fonts.stdFont);
		fontlwidth = Resources.fonts.stdFontlWidth;
		fontlheight = Resources.fonts.stdFontlHeight;
		fontCharacters = Resources.fonts.stdFontChars;
		// TODO Load KeyFace font
		
		// TITLE FONT
		titleFont = Art.enlargeImage(font, titleFontRatio);
		titleFontlwidth = fontlwidth * titleFontRatio;
		titleFontlheight = fontlheight * titleFontRatio;
		titleX = GameComponent.WIDTH / 2;
		titleY = GameComponent.HEIGHT / 4;
		
		super.init();
	}
	
	@Override
	public void draw(Graphics2D g) {
		// draw title
		Art.drawCenterString(g, titleFont, titleFontlwidth, titleFontlheight,
				fontCharacters, title, titleX, titleY);
		
		super.draw(g);
	}
	
	@Override
	public void update() {
		super.update();
	}

}
