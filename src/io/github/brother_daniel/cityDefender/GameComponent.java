/**
 * 
 */
package io.github.brother_daniel.cityDefender;

import java.awt.Canvas;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class GameComponent extends Canvas {
	private static final long serialVersionUID = 1L;

	public static String gameTitle = "City Defender";
	// Resolution of Game.
	public static int WIDTH = 854;
	public static int HEIGHT = 480;

	static InputHandler inputHandler;
	
	static Game game;

	/** Initialize game. **/
	public GameComponent() {
		setBounds(0, 0, WIDTH, HEIGHT);

		// Add InputHandler to canvas.
		inputHandler = new InputHandler();
		addKeyListener(inputHandler);
		addMouseListener(inputHandler);
		addMouseMotionListener(inputHandler);
		addFocusListener(inputHandler);
	}

	/** Initializes game and starts the game loop. **/
	public void start() {
		game = new Game(this, inputHandler);
		// Initialize or load game
		game.init();
		// Start Game loop
		game.start();
	}

	static WindowListener windowListener = new WindowListener() {
		@Override
		public void windowOpened(WindowEvent e) {
		}
		@Override
		public void windowIconified(WindowEvent e) {
		}
		@Override
		public void windowDeiconified(WindowEvent e) {
		}
		@Override
		public void windowDeactivated(WindowEvent e) {
		}
		@Override
		public void windowClosing(WindowEvent e) {
			// Stop the game when window starts closing
			game.stop();
		}
		@Override
		public void windowClosed(WindowEvent e) {
		}
		@Override
		public void windowActivated(WindowEvent e) {
		}
	};
	
	private static void displayFrame(Component compo) {
		// Initialize frame
		JFrame frame = new JFrame(getGameTitle());
		// Add component
		frame.add(compo);
		frame.addWindowListener(windowListener);
		frame.setSize(WIDTH, HEIGHT);
		// non of that non sense no sir
		frame.setResizable(false);
		// This may look confusing but it just centers the window
		frame.setLocationRelativeTo(null);

		// Setup inputHandler for the Frame.
		frame.addKeyListener(inputHandler);
		frame.addMouseListener(inputHandler);
		frame.addMouseMotionListener(inputHandler);
		frame.addFocusListener(inputHandler);

		// Show the frame
		frame.setVisible(true);

		// Transparent 16 x 16 pixel cursor image.
		BufferedImage cursorImg = new BufferedImage(16, 16,
				BufferedImage.TYPE_INT_ARGB);

		// Create a new blank cursor.
		Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
				cursorImg, new Point(0, 0), "blank cursor");

		// Set the blank cursor to the JFrame.
		frame.getContentPane().setCursor(blankCursor);
	}

	public static void main(String[] args) {
		// Initialize the Canvas
		GameComponent gc = new GameComponent();
		displayFrame(gc);
		gc.start();
	}

	public static String getGameTitle() {
		return gameTitle;
	}

	public static void setGameTitle(String gameTitle) {
		GameComponent.gameTitle = gameTitle;
	}

}
