/**
 * 
 */
package io.github.brother_daniel.cityDefender;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class Sound {

	public static Clip loadSound(final String resource) throws LineUnavailableException, UnsupportedAudioFileException, IOException {
		Clip clip = AudioSystem.getClip();
//		AudioInputStream inputStream = AudioSystem
		//				.getAudioInputStream(Sound.class.getResourceAsStream(resource));
		//		clip.open(inputStream);

		//		InputStream in = new BufferedInputStream(new FileInputStream(new File(
		//                Sound.class.getClassLoader()
		//                        .getResource(resource).getPath())));
		//        AudioInputStream audioStream = new AudioInputStream((TargetDataLine) in);
		//        Clip clip = AudioSystem.getClip();
		//		clip.open(audioStream);

		InputStream is = Sound.class.getClass().getResourceAsStream(resource);
		AudioInputStream inputStream = AudioSystem
				.getAudioInputStream(new BufferedInputStream(is));
		clip.open(inputStream);

		return clip;
	}

	public static synchronized Thread playSound(final Clip clip) {
		Thread thread;

		thread = new Thread(new Runnable() {
			public void run() {
				try {
					// set position to beginning
					clip.setMicrosecondPosition(0);
					// play clip from position.
					clip.start();
				} catch (Exception e) {
					System.out.println("Error playing sound you are probably "
							+ "using a jar file.");
					System.err.println(e.getMessage());
				}
			}
		});
		thread.start();
		return thread;
	}

}
