/**
 * 
 */
package io.github.brother_daniel.cityDefender;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 * 
 */
public class Art {

	public static int[][] loadImage(String resource) {
		try {
			// Read the image and set it to buffImg
			BufferedImage buffImg = ImageIO.read(Art.class
					.getResource(resource));
			// Get image dimensions
			int height = buffImg.getHeight();
			int width = buffImg.getWidth();
			int[][] pixels = new int[width][height];
			for (int row = 0; row < height; row++) {
				for (int col = 0; col < width; col++) {
					// get the colors of each pixel and set it to pixels[x][y]
					pixels[col][row] = buffImg.getRGB(col, row);
				}
			}
			return pixels;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param g
	 *            Graphics to draw with.
	 * @param font
	 *            Pixels of font to be drawn these pixels are loaded from
	 *            loadImage(resource);
	 * @param lwidth
	 *            Width of space containing each letter.
	 * @param lheight
	 *            Height of space containing each letter.
	 * @param characters
	 *            Map of characters in the font sheet.
	 * @param text
	 *            String to draw on to graphics.
	 * @param x
	 *            X position to draw string on to graphics.
	 * @param y
	 *            Y position to draw string on to graphics.
	 */
	public static void drawString(Graphics2D g, int[][] font, int lwidth, int lheight, String characters, String text, int x, int y) {
		for (int i = 0; i < text.length(); i++) {
			// Find what number the letter is
			int ch = characters.indexOf(text.charAt(i));
			if (ch < 0)
				continue;

			// Calculate what column the character is in by doing ch % 25.
			int column = ch % 26;
			// Calculate what row the character is in by doing ch / 25.
			int row = ch / 26;

			// extract the letter from font sheet
			int[][] letter = extractFromSheet(font, lwidth, lheight, column,
					row);
			// draw letter
			drawImage(g, x + i * lwidth, y, letter);
		}
	}

	public static void drawCenterString(Graphics2D g, int[][] font, int lwidth, int lheight, String characters, String text, int x, int y) {
		int centerY = y - lheight / 2;
		int centerX = x - lwidth * text.length() / 2;
		drawString(g, font, lwidth, lheight, characters, text, centerX, centerY);
	}

	public static int[][] extractFromSheet(int[][] sheet, int width, int height, int col, int row) {
		int offX = width * col;
		int offY = height * row;
		int[][] out = new int[width][height];
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				int sheetX = x + offX;
				int sheetY = y + offY;
				out[x][y] = sheet[sheetX][sheetY];
			}
		}
		return out;
	}

	public static void drawImage(Graphics2D g, int x, int y, int[][] pixels) {
		for (int ix = 0; ix < pixels.length; ix++) {
			for (int iy = 0; iy < pixels[ix].length; iy++) {
				Color pixel = new Color(pixels[ix][iy]);
				// If the pixel is the color of fuchsia
				if (pixel.getRed() == 255 && pixel.getGreen() == 0
						&& pixel.getBlue() == 128) {
					// do nothing/don't draw it...
				} else {
					// Set color to the color of the pixel
					g.setColor(pixel);
					// Draw a dot
					g.drawLine(x + ix, y + iy, x + ix, y + iy);
				}
			}
		}
	}

	/**
	 * @param pixels
	 *            original pixels to be replaced with.
	 * @param oldColor
	 *            oldColor to be changed to newColor.
	 * @param newColor
	 *            newColor to replace oldColor.
	 * @return replaced pixels.
	 */
	public static int[][] changeColor(final int[][] pixels, Color oldColor, Color newColor) {
		// variable to be returned
		int[][] out = new int[pixels.length][pixels[0].length];

		for (int x = 0; x < pixels.length; x++) {
			for (int y = 0; y < pixels[x].length; y++) {
				Color pixel = new Color(pixels[x][y]);
				// If pixel colour is equal to the old color we are looking
				// to change then...
				if (pixel.getRGB() == oldColor.getRGB()) {
					// Change colour.
					out[x][y] = newColor.getRGB();
				} else {
					// keep it the same colour.
					out[x][y] = pixels[x][y];
				}
			}
		}
		return out;
	}

	public static int[][] enlargeImage(final int[][] pixels, int xRatio, int yRatio) {
		int[][] out;
		out = new int[pixels.length * xRatio][pixels[0].length * yRatio];

		for (int x = 0; x < pixels.length; x++) {
			for (int y = 0; y < pixels[x].length; y++) {
				
				for (int xOff = 0; xOff < xRatio; xOff++) {
					for (int yOff = 0; yOff < yRatio; yOff++) {
						out[x * xRatio + xOff][y * yRatio + yOff] = pixels[x][y];
					}
				}
				
			}
		}

		return out;
	}
	
	public static int[][] enlargeImage(final int[][] pixels, int ratio) {
		return (enlargeImage(pixels, ratio, ratio));
	}

}
