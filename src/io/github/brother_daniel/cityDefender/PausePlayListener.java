/**
 * 
 */
package io.github.brother_daniel.cityDefender;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Fitzgerald <Brother-Daniel@users.noreply.github.com>
 *
 */
public interface PausePlayListener {
	public void onResume();
	public void onPause();
}

class PausePlayInit {
	private List<PausePlayListener> listeners = new ArrayList<PausePlayListener>();

	public void addListener(PausePlayListener toAdd) {
		listeners.add(toAdd);
	}

	public void resume() {
		for (PausePlayListener ppl : listeners)
			ppl.onResume();
	}
	
	public void pause() {
		for (PausePlayListener ppl : listeners)
			ppl.onPause();
	}
}
